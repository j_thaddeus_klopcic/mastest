﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DataLayer
{
    public class EmployeesTestApiClient
    {
        private string _masTestUrl = "http://masglobaltestapi.azurewebsites.net/api/";
        private string _masTestEmployees = "Employees";

        public List<dynamic> GetEmployees()
        {
            try
            {
                var webClient = new WebClient();
                var data = webClient.DownloadString(_masTestUrl + _masTestEmployees);

                var employees = JArray.Parse(data);

                return employees.ToObject<List<dynamic>>();
            }
            catch (Exception ex)
            {
                // We would log the error here
                throw;
            }
        }
    }
}
