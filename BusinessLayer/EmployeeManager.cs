﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;

namespace BusinessLayer
{
    public class EmployeeManager
    {
        private EmployeesTestApiClient _employeeClient = new EmployeesTestApiClient();

        public EmployeeManager()
        {

        }

        public List<Employee> GetEmployees()
        {
            var employees = new List<Employee>();

            var dataList = _employeeClient.GetEmployees();

            foreach (var data in dataList)
            {
                employees.Add(EmployeeFactory.CreateEmployee(data));
            }

            return employees;
        }

        public Employee GetEmployee(int id)
        {
            return GetEmployees().FirstOrDefault(e => e.Id == id);
        }

    }
}
