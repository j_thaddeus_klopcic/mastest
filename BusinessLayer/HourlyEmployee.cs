﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class HourlyEmployee : Employee
    {
        public override int AnnualSalary
        {
            get { return 120 * HourlySalary.Value * 12; }
        }

        public HourlyEmployee(dynamic data) : base((object)data)
        {
            HourlySalary = data.hourlySalary;
        }
    }
}
