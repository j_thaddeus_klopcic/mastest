﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class MonthlyEmployee : Employee
    {
        public override int AnnualSalary
        {
            get {return MonthlySalary.Value* 12;}
        }

        public MonthlyEmployee(dynamic data) : base((object)data)
        {
            MonthlySalary = data.monthlySalary;
        }
    }
}
