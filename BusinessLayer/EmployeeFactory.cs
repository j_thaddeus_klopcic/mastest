﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    class EmployeeFactory
    {
        public static Employee CreateEmployee(dynamic data)
        {
            try
            {
                string type = data.contractTypeName;

                Employee employee = null;

                switch (type)
                {
                    case "HourlySalaryEmployee":
                        {
                            employee = new HourlyEmployee(data);
                            break;
                        }

                    case "MonthlySalaryEmployee":
                        {
                            employee = new MonthlyEmployee(data);
                            break;
                        }

                    default:
                        {
                            employee = null;
                            break;
                        }
                }

                return employee;
            }
            catch(Exception ex)
            {
                // Log error here
                return null;
            }
        }
    }
}
