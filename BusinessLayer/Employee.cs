﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    // Base class for Employees
    public abstract class Employee
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public string ContractType { get; protected set; }

        public int RoleId { get; protected set; }

        public string RoleDescription { get; protected set; }

        public int? HourlySalary { get; protected set; }

        public int? MonthlySalary { get; protected set; }

        public virtual int AnnualSalary {get;}


        public Employee(dynamic data)
        {
            Id = data.id;
            Name = data.name;
            ContractType = data.contractTypeName;
            RoleId = data.roleId;
            RoleDescription = data.roleDescription;
        }
    }
}
