using DataLayer;
using BusinessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestApiClientGetEmployees()
        {
            var employeesClient = new EmployeesTestApiClient();

            var employees = employeesClient.GetEmployees();

            Assert.IsNotNull(employees);
            Assert.IsTrue(employees.Count > 0);
        }


        [TestMethod]
        public void TestEmpManagerGetEmployees()
        {
            var employeesManager = new EmployeeManager();

            var employees = employeesManager.GetEmployees();

            Assert.IsNotNull(employees);
            Assert.IsTrue(employees.Count > 0);
            Assert.IsFalse(employees.Any(e => e == null));
        }

        [TestMethod]
        public void TestEmpManagerGetEmployee()
        {
            var employeesManager = new EmployeeManager();

            var employee1 = employeesManager.GetEmployee(1);

            Assert.IsNotNull(employee1);
            Assert.IsTrue(employee1.Id == 1);
            Assert.IsFalse(employee1.MonthlySalary.HasValue);
            Assert.IsTrue(employee1.AnnualSalary == employee1.HourlySalary * 12 * 120);

            var employee2 = employeesManager.GetEmployee(2);

            Assert.IsNotNull(employee2);
            Assert.IsTrue(employee2.Id == 2);
            Assert.IsFalse(employee2.HourlySalary.HasValue);
            Assert.IsTrue(employee2.AnnualSalary == employee2.MonthlySalary * 12);
        }

    }
}
